==================
Contributing Guide
==================

Contributions are what make the open source community such an amazing place to
learn, inspire, and create in. Any contributions you make are **greatly appreciated!**


TL;DR Quickstart
================

#. Have pre-requisites installed:

   * `git <https://git-scm.com/book/en/v2/Getting-Started-Installing-Git>`__
   * `Python 3.7+ <https://realpython.com/installing-python/>`__
   * `Set up your local preview environment`_ by installing ``nox`` and ``pre-commit``

#. Fork the project
#. ``git clone`` your fork locally
#. Create your feature branch (ex. ``git checkout -b amazing-feature``)
#. Run ``pre-commit install``
#. Test out ``nox`` and build docs with ``nox -e 'docs-html(clean=True)'``. The first time this is ran will take some time, but later generating of docs can be done with ``nox -e 'docs-html(clean=False)'``
#. Hack away!
#. Run all unit tests with ``nox -e tests-3 -- -vv tests/unit/idem_aws``

   * ``tests-3`` ensures that the currently available version of Python 3 is ran (if supported)
   * Run targeted unit tests by calling a file directly. Example: ``nox -e tests-3 -- -vv tests/unit/idem_aws/tool/aws/test_client.py``

#. Commit your changes (ex. ``git commit -m 'Add some amazing-feature'``)
#. Push to the branch (ex. ``git push origin amazing-feature``)
#. Open a merge request

For the full details, see below.


Ways to contribute
==================

We value all contributions, not just contributions to the code. In addition to
contributing to the code, you can help the project by:

* Writing, reviewing, and revising documentation, modules, and tutorials
* Opening issues on bugs, feature requests, or docs
* Spreading the word about how great this project is

The rest of this guide will explain our toolchain and how to set up your
environment to contribute to the project.


Overview of how to contribute to this repository
================================================

To contribute to this repository, you first need to set up your own local repository:

* `Fork, clone, and branch the repo`_
* `Set up your local preview environment`_

After this initial setup, you then need to:

* `Sync local master branch with upstream master`_
* Update code and relevant docstrings
* Update related documentation in `reStructuredText (rST) <https://saltstack.gitlab.io/open/docs/docs-hub/topics/rst-guide.html>`__
* `Preview docs changes locally`_
* Open a MR

Once a merge request gets approved, it can be merged!


Prerequisites
=============

For local development, the following prerequisites are needed:

* `git <https://git-scm.com/book/en/v2/Getting-Started-Installing-Git>`__
* `Python 3.7+ <https://realpython.com/installing-python/>`__
* `Ability to create python venv <https://realpython.com/python-virtual-environments-a-primer/>`__

This project makes use of **POP**, or *Plugin Oriented Programming*, in addition to **idem**.
If you are new to projects making use of these, the following reference material should be
useful:

* `Intro to Plugin Oriented Programming (POP) <https://pop-book.readthedocs.io/en/latest/>`__
* `pop reference docs <https://pop.readthedocs.io/>`__
* `idem reference docs <https://idem.readthedocs.io/>`__
* `Idem Project Website <https://www.idemproject.io/>`__
* `Idem Project docs portal <https://docs.idemproject.io/>`__


Windows 10/11 users
-------------------

For the best experience, when contributing from a Windows OS to projects using
Python-based tools like ``pre-commit``, we recommend setting up `Windows Subsystem
for Linux (WSL) <https://docs.microsoft.com/en-us/windows/wsl/>`__, with the
latest version being WSLv2.

The following gists on GitHub have been consulted with success for several
contributors:

* `Official Microsoft docs on installing WSL <https://docs.microsoft.com/en-us/windows/wsl/install-win10>`__

* A list of PowerShell commands in a gist to `Enable WSL and Install Ubuntu 20.04
  <https://gist.github.com/ScriptAutomate/f94cd44dacd0f420fae65414e717212d>`__

  * Ensure you also read the comment thread below the main content for
    additional guidance about using Python on the WSL instance.

We recommend `Installing Chocolatey on Windows 10 via PowerShell w/ Some Starter Packages
<https://gist.github.com/ScriptAutomate/02e0cf33786f869740ee963ed6a913c1>`__.
This installs ``git``, ``microsoft-windows-terminal``, and other helpful tools via
the awesome Windows package management tool, `Chocolatey <https://chocolatey.org/why-chocolatey>`__.

``choco install git`` easily installs ``git`` for a good Windows-dev experience.
From the ``git`` package page on Chocolatey, the following are installed:

* Git BASH
* Git GUI
* Shell Integration


Fork, clone, and branch the repo
================================

This project uses the fork and branch Git workflow. For an overview of this method,
see
`Using the Fork-and-Branch Git Workflow <https://blog.scottlowe.org/2015/01/27/using-fork-branch-git-workflow/>`__.

* First, create a new fork into your personal user space.
* Then, clone the forked repo to your local machine.

  .. code-block:: bash

     # SSH or HTTPS
     git clone <forked-repo-path>/idem_aws.git

.. note::

    Before cloning your forked repo when using SSH, you need to create an SSH
    key so that your local Git repository can authenticate to the GitLab remote server.
    See `GitLab and SSH keys <https://docs.gitlab.com/ee/ssh/README.html>`__ for instructions,
    or `Connecting to GitHub with SSH <https://docs.github.com/en/github-ae@latest/github/authenticating-to-github/connecting-to-github-with-ssh>`__.

Configure the remotes for your main upstream repository:

.. code-block:: bash

    # Move into cloned repo
    cd idem_aws

    # Choose SSH or HTTPS upstream endpoint
    # git@gitlab.com:vmware/idem/idem-aws.git
    # https://gitlab.com/vmware/idem/idem-aws.git
    # Example using SSH:
    git remote add upstream git@gitlab.com:vmware/idem/idem-aws.git

Create new branch for changes to submit:

.. code-block:: bash

    git checkout -b amazing-feature


Set up your local preview environment
=====================================

If you are not on a Linux machine, you need to set up a virtual environment to
preview your local changes and ensure the `prerequisites`_ are met for a Python
virtual environment.

From within your local copy of the forked repo:

.. code-block:: bash

    # Install required python packages to venv
    # This may require pip3 instead of pip in commands
    pip install --user --upgrade pip
    pip install --user --upgrade nox pre-commit

    # Setup pre-commit by running this in root of project dir
    pre-commit install


``pre-commit`` and ``nox`` Setup
--------------------------------

This project uses `pre-commit <https://pre-commit.com/>`__ and
`nox <https://nox.thea.codes/en/stable/>`__ to make it easier for
contributors to get quick feedback, for quality control, and to increase
the chance that your merge request will get reviewed and merged.

``nox`` handles Sphinx requirements and plugins for you, always ensuring your
local packages are the needed versions when building docs. You can think of it
as ``Make`` with superpowers.

``nox`` also manages dynamic virtual environments for running tests, instead of
requiring contributors to setup their own environments to run ``pytest`` within.


What is pre-commit?
-------------------

``pre-commit`` is a tool that will automatically run
local tests when you attempt to make a git commit. To view what tests are run,
you can view the ``.pre-commit-config.yaml`` file at the root of the
repository.

One big benefit of pre-commit is that *auto-corrective measures* can be done
to files that have been updated. This includes Python formatting best
practices, proper file line-endings (which can be a problem with repository
contributors using differing operating systems), and more.

If an error is found that cannot be automatically fixed, error output will help
point you to where an issue may exist.


Sync local master branch with upstream master
=============================================

If needing to sync feature branch with changes from upstream master, do the
following:

.. note::

    This will need to be done in case merge conflicts need to be resolved
    locally before a merge to master in the upstream repo.

.. code-block:: bash

    git checkout master
    git fetch upstream
    git pull upstream master
    git push origin master
    git checkout my-new-feature
    git merge master


Preview docs changes locally
============================

To ensure that the changes you are implementing are formatted correctly, you
should preview a local build of your changes first. To preview the changes:

.. code-block:: bash

    # Generate HTML documentation with nox
    nox -e 'docs-html(clean=True)' # First run
    nox -e 'docs-html(clean=False)' # Subsequent runs

    # Sphinx website documentation is dumped to docs/_build/html/*
    # View locally
    # Use xdg-open instead of firefox when on Linux and MacOS systems
    firefox docs/_build/html/index.html # Firefox is just an example

    # Optional: Runs an interactive docs site while modifying
    nox -e docs

.. note::

    If you encounter an error, Sphinx may be pointing out formatting errors
    that need to be resolved in order for ``nox`` to properly generate the docs.


Testing
=======

Unit tests can be run with only the prerequisite of ``nox``.

.. code:: bash

    # Example: Run all unit tests
    nox -e tests-3 -- -vv tests/unit/idem_aws

Integration tests will require LocalStack.


LocalStack setup
----------------

`LocalStack <https://localstack.cloud/>`__ can be used to test ``idem-aws`` on your local machine
without needing legitimate AWS credentials. It can be used for running the ``idem-aws`` tests
or for testing your states locally.

* `Getting Started with LocalStack CLI <https://docs.localstack.cloud/get-started/#localstack-cli>`__

.. note::

    To test with LocalStack CLI, `docker <https://docs.docker.com/get-docker/>`__ is required.

Install LocalStack with ``pip`` in a ``venv``:

.. code:: bash

    # Create a new virtualenv
    # May require python3 instead of python
    python -m venv .venv
    source .venv/bin/activate
    pip install -U pip setuptools wheel

    # Install localstack
    pip install "localstack [full]"

Start the LocalStack infrastructure:

.. code:: bash

    localstack infra start

In a separate terminal, you should now be able to run integration tests.


Run Integration Tests
+++++++++++++++++++++

In order to run the tests you must have a profile called ``test_development_idem_aws`` in your ``acct`` provider
information. This can use LocalStack (recommended), otherwise testing does support
valid production AWS credentials **at your own risk**.

Create a file named ``.localstack.yml`` with the following contents. This file will be ignored by the
``.gitignore`` file in order to prevent any accidental commits of your localstack configuration.

.. code:: yml

    aws:
      test_development_idem_aws:
        endpoint_url: http://localhost:4566
        use_ssl: False
        aws_access_key_id: localstack
        aws_secret_access_key: _
        region_name: us-west-1

For tests to properly reference the profile, you need to define the ``ACCT_FILE`` environment
variable. Integration tests can then be ran as expected.

.. code:: bash

    # ACCT_FILE is referenced by idem-aws to know what profile you are using
    export ACCT_FILE='.localstack.yml'

    # Example: Run all integration tests
    nox -e tests-3 -- -vv tests/integration/idem_aws

    # Example: Run all integration AND unit tests
    nox -e tests-3 -- -vv tests/integration/idem_aws tests/unit/idem_aws
