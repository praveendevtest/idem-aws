# Find leftover Instances
orphan_instances:
  exec.run:
    - path: aws.ec2.instance.list
    - kwargs:
        name: null
        filters:
          - Name: "tag:Name"
            Values:
              - "idem-fixture-instance-*"

#!require:orphan_instances
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan_instances}') %}

# Remove leftover Instances
cleanup-{{ resource['resource_id'] }}:
  aws.ec2.instance.absent:
    - resource_id: {{ resource['resource_id'] }}

{% endfor %}
#!END

#!require:orphan_subnets
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan_subnets}') %}

orphan-instance-{{ resource['resource_id'] }}:
  exec.run:
    - path: aws.ec2.instance.get
    - kwargs:
        name: null
        filters:
          - Name: subnet-id
            Values:
              - {{ resource['resource_id'] }}

# Remove instances that rely on test subnets
cleanup-instance-{{ resource['resource_id'] }}:
  aws.ec2.instance.absent:
    - resource_id: ${aws.ec2.instance:orphan-instance-{{ resource['resource_id'] }}:resource_id}

{% endfor %}
