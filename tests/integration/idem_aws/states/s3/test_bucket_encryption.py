import copy
import time
from collections import ChainMap

import pytest
from flaky import flaky

STATE_NAME = "aws.s3.bucket_encryption"


@pytest.mark.flaky(max_runs=5)
@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_bucket_encryption(hub, ctx, aws_s3_bucket, aws_kms_key):
    bucket_encryption_temp_name = "idem-test-bucket-encryption-" + str(int(time.time()))
    bucket = aws_s3_bucket.get("name")
    server_side_encryption_configuration = {
        "Rules": [
            {
                "ApplyServerSideEncryptionByDefault": {"SSEAlgorithm": "AES256"},
                "BucketKeyEnabled": False,
            }
        ]
    }
    new_server_side_encryption_configuration = {
        "Rules": [
            {
                "ApplyServerSideEncryptionByDefault": {
                    "SSEAlgorithm": "aws:kms",
                    "KMSMasterKeyID": aws_kms_key.get("resource_id"),
                },
                "BucketKeyEnabled": True,
            }
        ]
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create bucket encryption with test flag
    ret = await hub.states.aws.s3.bucket_encryption.present(
        test_ctx,
        name=bucket_encryption_temp_name,
        bucket=bucket,
        server_side_encryption_configuration=server_side_encryption_configuration,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type=STATE_NAME, name=bucket_encryption_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert bucket_encryption_temp_name == resource.get("name")
    assert bucket == resource.get("bucket")
    assert server_side_encryption_configuration == resource.get(
        "server_side_encryption_configuration"
    )

    # Create bucket encryption
    ret = await hub.states.aws.s3.bucket_encryption.present(
        ctx,
        name=bucket_encryption_temp_name,
        bucket=bucket,
        server_side_encryption_configuration=server_side_encryption_configuration,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type=STATE_NAME, name=bucket_encryption_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert bucket_encryption_temp_name == resource.get("name")
    assert bucket == resource.get("resource_id")
    assert bucket == resource.get("bucket")
    assert server_side_encryption_configuration == resource.get(
        "server_side_encryption_configuration"
    )

    resource_id = resource.get("resource_id")

    # Verify that the created bucket encryption is present (describe)
    ret = await hub.states.aws.s3.bucket_encryption.describe(ctx)

    resource_name = f"{resource_id}-encryption"
    assert resource_name in ret
    assert f"{STATE_NAME}.present" in ret.get(resource_name)
    resource = ret.get(resource_name).get(f"{STATE_NAME}.present")
    resource_map = dict(ChainMap(*resource))
    assert resource_name == resource_map.get("name")
    assert resource_id == resource_map.get("resource_id")
    assert bucket == resource_map.get("bucket")
    assert server_side_encryption_configuration == resource_map.get(
        "server_side_encryption_configuration"
    )

    # Create bucket encryption again with different bucket name and resource_id with test flag
    ret = await hub.states.aws.s3.bucket_encryption.present(
        test_ctx,
        name=bucket_encryption_temp_name,
        bucket=bucket,
        server_side_encryption_configuration=server_side_encryption_configuration,
        resource_id=bucket_encryption_temp_name,
    )

    assert not ret["result"] and ret["comment"]
    assert (
        f"Bucket '{bucket}' and resource_id '{bucket_encryption_temp_name}' parameters must be the same"
        in ret["comment"]
    )
    assert not ret.get("old_state") and not ret.get("new_state")

    # Create bucket encryption again with different bucket name and resource_id
    ret = await hub.states.aws.s3.bucket_encryption.present(
        ctx,
        name=bucket_encryption_temp_name,
        bucket=bucket,
        server_side_encryption_configuration=server_side_encryption_configuration,
        resource_id=bucket_encryption_temp_name,
    )

    assert not ret["result"] and ret["comment"]
    assert (
        f"Bucket '{bucket}' and resource_id '{bucket_encryption_temp_name}' parameters must be the same"
        in ret["comment"]
    )
    assert not ret.get("old_state") and not ret.get("new_state")

    # Create bucket encryption again with same resource_id and no change in state with test flag
    ret = await hub.states.aws.s3.bucket_encryption.present(
        test_ctx,
        name=bucket_encryption_temp_name,
        bucket=bucket,
        server_side_encryption_configuration=server_side_encryption_configuration,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type=STATE_NAME,
            name=bucket_encryption_temp_name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Create bucket encryption again with same resource_id and no change in state
    ret = await hub.states.aws.s3.bucket_encryption.present(
        ctx,
        name=bucket_encryption_temp_name,
        bucket=bucket,
        server_side_encryption_configuration=server_side_encryption_configuration,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type=STATE_NAME,
            name=bucket_encryption_temp_name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Update bucket encryption with test flag
    ret = await hub.states.aws.s3.bucket_encryption.present(
        test_ctx,
        name=bucket_encryption_temp_name,
        bucket=bucket,
        server_side_encryption_configuration=new_server_side_encryption_configuration,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type=STATE_NAME,
            name=bucket_encryption_temp_name,
        )[0]
        in ret["comment"]
    )
    assert (
        hub.tool.aws.comment_utils.would_update_comment(
            resource_type=STATE_NAME, name=bucket_encryption_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert bucket_encryption_temp_name == resource.get("name")
    assert bucket_encryption_temp_name == resource.get("name")
    assert bucket == resource.get("bucket")
    assert new_server_side_encryption_configuration == resource.get(
        "server_side_encryption_configuration"
    )

    # Update bucket encryption
    ret = await hub.states.aws.s3.bucket_encryption.present(
        ctx,
        name=bucket_encryption_temp_name,
        bucket=bucket,
        server_side_encryption_configuration=new_server_side_encryption_configuration,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type=STATE_NAME,
            name=bucket_encryption_temp_name,
        )[0]
        in ret["comment"]
    )
    assert (
        hub.tool.aws.comment_utils.update_comment(
            resource_type=STATE_NAME, name=bucket_encryption_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert bucket_encryption_temp_name == resource.get("name")
    assert bucket == resource.get("bucket")
    assert new_server_side_encryption_configuration == resource.get(
        "server_side_encryption_configuration"
    )

    # Delete bucket encryption with test flag
    ret = await hub.states.aws.s3.bucket_encryption.absent(
        test_ctx,
        name=bucket_encryption_temp_name,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type=STATE_NAME, name=bucket_encryption_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert bucket_encryption_temp_name == resource.get("name")
    assert bucket == resource.get("bucket")

    # Delete bucket encryption
    ret = await hub.states.aws.s3.bucket_encryption.absent(
        ctx,
        name=bucket_encryption_temp_name,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type=STATE_NAME, name=bucket_encryption_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert bucket_encryption_temp_name == resource.get("name")
    assert bucket == resource.get("bucket")

    # Delete the same bucket encryption again (deleted state) will not invoke delete on AWS side
    ret = await hub.states.aws.s3.bucket_encryption.absent(
        ctx,
        name=bucket_encryption_temp_name,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=STATE_NAME, name=bucket_encryption_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])

    # Delete bucket encryption with no resource_id will consider it as absent
    ret = await hub.states.aws.s3.bucket_encryption.absent(
        ctx, name=bucket_encryption_temp_name
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=STATE_NAME, name=bucket_encryption_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])
